import React, {useEffect, useState} from "react";
import Input from "../../components/Input";
import styles from './Home.module.scss'
import RepositoryItem from "../../components/RepositoryItem";
import useRepositories from "../../hooks/useRepositories";
import {useDebounce} from "../../hooks/useDebouns";

const DELAY = 500 //ms

export const Home = () => {
    const [inputValue, setInputValue] = useState('')
    const [prevValues, setPrevValues] = useState([]);

    const {getAllRepositories, result} = useRepositories();

    const debouncedText = useDebounce(inputValue, DELAY);

    useEffect(() => {
        if (debouncedText) {
            getAllRepositories(debouncedText)
            if (!prevValues.includes(debouncedText)) {
                const values = [...prevValues, debouncedText].slice(-5)
                setPrevValues(values)
                localStorage.setItem('items', JSON.stringify(values))
            }
        }
    }, [debouncedText])

    useEffect(() => {
        const items = localStorage.getItem('items');
        let parcedItems = []

        if (items) {
            parcedItems = JSON.parse(items)
            setPrevValues(parcedItems)
        }
    }, [])

    return (
        <div className={styles.container}>
            <div className={styles.side}>
                <Input value={inputValue} onChange={(e) => setInputValue(e.target.value)}/>
                <p>Search history:</p>
                <ul>
                    {prevValues.map((item, index) => <li key={`searchItem${index}`}>{item}</li>)}
                </ul>
            </div>
            <div className={styles.itemsContainer}>
                {result?.map((item, index) =>
                    <RepositoryItem
                        key={`RepositoryItem${index}`}
                        name={item.name}
                        description={item.description}
                        url={item.owner.html_url}
                        language={item.language}
                    />
                )}
            </div>
        </div>
    )
}