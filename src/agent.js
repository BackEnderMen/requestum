import superagentPromise from 'superagent-promise';
import _superagent from 'superagent';

const superagent = superagentPromise(_superagent, global.Promise);
const ROOT = process.env.REACT_APP_URL || `https://api.github.com`;
const API_ROOT = `${ROOT}/`;
let token = '';

const handleErrors = (err) => {
    if (err && err.response && err.response.status === 401) {
    }
    return err;
};

const responseBody = (res) => {
    /* eslint-disable-next-line no-proto */
    if (res.headers['x-total-count']) res.body.__proto__.count = res.headers['x-total-count'];
    return res.body;
};

const tokenPlugin = (req) => {
    if (token) {
        req.set('Authorization', `Bearer ${token}`);
    }
};

const requests = {
    del: (url, root = API_ROOT) => superagent
        .del(`${root}${url}`)
        .use(tokenPlugin)
        .end(handleErrors)
        .then(responseBody),
    get: (url, root = API_ROOT, addCount) => superagent
        .get(`${root}${url}`)
        .use(tokenPlugin)
        
        //
        
         .set('Access-Control-Expose-Headers', 'X-Total-Count,X-Total-count')
        .end(handleErrors)
        .then((res) => responseBody(res, addCount)),
    put: (url, body) => superagent
        .put(`${API_ROOT}${url}`, body)
        .use(tokenPlugin)
        .end(handleErrors)
        .then(responseBody),
    post: (url, body, root = API_ROOT) => superagent
        .post(`${root}${url}`, body)
        .use(tokenPlugin)
        .end(handleErrors)
        .then(responseBody),
    patch: (url, body, root = API_ROOT) => superagent
        .patch(`${root}${url}`, body)
        .use(tokenPlugin)
        .end(handleErrors)
        .then(responseBody),
    delete: (url, root = API_ROOT) => superagent
        .del(`${root}${url}`)
        .use(tokenPlugin)
        .end(handleErrors)
        .then(responseBody),
};


const Repositories = {
    all: (termString, limit = 20, offset = 0) => requests.get(`search/repositories?q=${termString}&sort=stars&order=desc&per_page=${limit}&page=${offset}`)
};

// eslint-disable-next-line import/no-anonymous-default-export
export default {
    Repositories,
};
