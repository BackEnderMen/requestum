import React from "react";
import styles from './Input.module.scss'
import PropTypes from "prop-types";

export const Input = ({onChange, value, placeholder}) => {
    return (
        <input
            onChange={onChange}
            name="input"
            type="text"
            className={styles.input}
            placeholder={placeholder}
            value={value}
        />
    )
}
Input.propTypes = {
    onChange: PropTypes.func,
    value: PropTypes.string,
    placeholder: PropTypes.string,
};

Input.defaultProps = {
    onChange: () => {
    },
    value: '',
    placeholder: '',
};
