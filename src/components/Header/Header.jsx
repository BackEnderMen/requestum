import React from "react";
import styles from "./Header.module.scss"
import Logo from '../../logo.svg';

export const Header = () => {
    return (
        <header className={styles.header}>
            <div className={styles.requestLogo}>
                <img className={styles.logo} alt={'img'} src={Logo}/>
                <div>
                    <p className={styles.titleText}>
                        requestum
                    </p>
                    <p>
                        web development company
                    </p>
                </div>
            </div>
            <p>GitHub users search app</p>
        </header>
    )
};
